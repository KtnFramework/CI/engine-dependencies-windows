$bullet_version = "3.17"
$bullet_extracted_name = "bullet3-$bullet_version"
$bullet_archive = "$bullet_version.zip"
$bullet_download_target = "$PWD/$bullet_archive"
$bullet_url = "https://github.com/bulletphysics/bullet3/archive/refs/tags/$bullet_archive"
if (!([IO.Directory]::Exists((Join-Path(Get-Location) 'bullet')))) {   
    if (Test-Path $bullet_extracted_name) {
        Remove-Item $bullet_extracted_name -Recurse -Force
    }
    if (Test-Path $bullet_archive) {
        Remove-Item $bullet_archive
    }
    Write-Output "Downloading bullet..."
    (New-Object System.Net.WebClient).DownloadFile($bullet_url, $bullet_download_target); if (!$?) {
        Write-Output "Could not download bullet."; exit 1
    }
    7z x $bullet_archive; if (!$?) {
        Write-Output "Could not extract $bullet_archive."; exit 1
    }
    Remove-Item $bullet_archive
    Rename-Item $bullet_extracted_name bullet
}



Set-Location bullet
if (!(Test-Path build)) {
    mkdir build
}
Set-Location build
cmake `
    -A x64 `
    -DBUILD_BULLET2_DEMOS=OFF `
    -DBUILD_CPU_DEMOS=OFF `
    -DBUILD_EXTRAS=OFF `
    -DBUILD_UNIT_TESTS=OFF `
    -DCMAKE_DEBUG_POSTFIX="" `
    -DUSE_GRAPHICAL_BENCHMARK=OFF `
    -DUSE_MSVC_RUNTIME_LIBRARY_DLL=ON `
    -T host=x64 `
    ..
MSBuild BULLET_PHYSICS.sln /m
MSBuild BULLET_PHYSICS.sln /m /p:Configuration=Release
Set-Location ../..



$build_date = Get-Date -Format yyyy_MM_dd
$package_name = "bullet-$bullet_version-$build_date"
if (([IO.Directory]::Exists((Join-Path(Get-Location) $package_name)))) {
    Remove-Item -Force -Recurse $package_name
}
mkdir $package_name
Copy-Item bullet/build/lib -Recurse -Destination $package_name
Copy-Item bullet/src -Recurse -Destination $package_name
Set-Location $package_name
7z a -r "../$package_name.7z"
Set-Location ..
Remove-Item -Force -Recurse $package_name
Write-Output "Use `"7z e $package_name.7z -o<OutputFolder>`" to unzip."
