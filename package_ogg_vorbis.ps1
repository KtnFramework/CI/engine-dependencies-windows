$libogg_version = "1.3.5"
$libogg_extracted_name = "libogg-$libogg_version"
$libogg_archive = "$libogg_extracted_name.zip"
$libogg_download_target = "$PWD/$libogg_archive"
$libogg_url = "https://github.com/xiph/ogg/releases/download/v$libogg_version/$libogg_archive"
if (!([IO.Directory]::Exists((Join-Path(Get-Location) 'libogg')))) {   
    if (Test-Path $libogg_extracted_name) {
        Remove-Item $libogg_extracted_name -Recurse -Force
    }
    if (Test-Path $libogg_archive) {
        Remove-Item $libogg_archive
    }
    Write-Output "Downloading libogg..."
    (New-Object System.Net.WebClient).DownloadFile($libogg_url, $libogg_download_target); if (!$?) {
        Write-Output "Could not download libogg."; exit 1
    }
    7z x $libogg_archive; if (!$?) {
        Write-Output "Could not extract $libogg_archive."; exit 1
    }
    Remove-Item $libogg_archive
    Rename-Item $libogg_extracted_name libogg
}

Set-Location libogg
if (!(Test-Path build)) {
    mkdir build
}
Set-Location build
cmake `
    -DBUILD_SHARED_LIBS=ON `
    -DBUILD_TESTING=OFF `
    -DCMAKE_BUILD_TYPE=Release `
    -DCMAKE_C_COMPILER=clang `
    -DCMAKE_CXX_COMPILER=clang++ `
    -DCMAKE_RC_COMPILER=llvm-rc `
    -GNinja `
    ..
cmake --build .
Set-Location ../..



$libvorbis_version = "1.3.7"
$libvorbis_extracted_name = "libvorbis-$libvorbis_version"
$libvorbis_archive = "$libvorbis_extracted_name.zip"
$libvorbis_download_target = "$PWD/$libvorbis_archive"
$libvorbis_url = "https://github.com/xiph/vorbis/releases/download/v$libvorbis_version/$libvorbis_archive"
if (!([IO.Directory]::Exists((Join-Path(Get-Location) 'libvorbis')))) {
    if (Test-Path $libvorbis_extracted_name) {
        Remove-Item $libvorbis_extracted_name -Recurse -Force
    }
    if (Test-Path $libvorbis_archive) {
        Remove-Item $libvorbis_archive
    }
    Write-Output "Downloading libvorbis..."
    (New-Object System.Net.WebClient).DownloadFile($libvorbis_url, $libvorbis_download_target); if (!$?) {
        Write-Output "Could not download libvorbis."; exit 1
    }
    7z x $libvorbis_archive; if (!$?) {
        Write-Output "Could not extract $libvorbis_archive."; exit 1
    }
    Remove-Item $libvorbis_archive
    Rename-Item $libvorbis_extracted_name libvorbis
}

Set-Location libvorbis
if (!(Test-Path -path build)) {
    mkdir build
}
Set-Location build
cmake `
    -DBUILD_SHARED_LIBS=ON `
    -DCMAKE_BUILD_TYPE=Release `
    -DCMAKE_C_COMPILER=clang `
    -DCMAKE_CXX_COMPILER=clang++ `
    -DCMAKE_RC_COMPILER=llvm-rc `
    -DOGG_INCLUDE_DIR='$PWD/../../../libogg/include' `
    -DOGG_LIBRARY='$PWD/../../../libogg/build/ogg.lib' `
    -GNinja `
    ..
cmake --build .
Set-Location ../..



$build_date = Get-Date -Format yyyy_MM_dd
$package_name = "ogg-$libogg_version-vorbis-$libvorbis_version-$build_date"
if (([IO.Directory]::Exists((Join-Path(Get-Location) $package_name)))) {
    Remove-Item -Force -Recurse $package_name
}
mkdir $package_name/include/ogg
mkdir $package_name/include/vorbis

Copy-Item libogg/include/ogg/*.h -Destination $package_name/include/ogg
Copy-Item libogg/build/*.dll -Destination $package_name
Copy-Item libogg/build/*.lib -Destination $package_name

Copy-Item libvorbis/include/vorbis/*.h -Destination $package_name/include/vorbis
Copy-Item libvorbis/build/lib/*.dll -Destination $package_name
Copy-Item libvorbis/build/lib/*.lib -Destination $package_name

Set-Location $package_name
7z a -r "../$package_name.7z"
Set-Location ..
Remove-Item -Force -Recurse $package_name
Write-Output "Use `"7z e $package_name.7z -o<OutputFolder>`" to unzip."
