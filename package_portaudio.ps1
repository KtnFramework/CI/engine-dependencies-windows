$portaudio_version = "v190700_20210406"
$portaudio_name = "pa_stable_" + $portaudio_version
$portaudio_archive = $portaudio_name + ".tgz"
$portaudio_url = "http://files.portaudio.com/archives/$portaudio_archive"
if (!([IO.Directory]::Exists((Join-Path(Get-Location) 'portaudio')))) {
    if (Test-Path $portaudio_archive) {
        Remove-Item $portaudio_archive
    }
    if (Test-Path "$portaudio_name.tar") {
        Remove-Item "$portaudio_name.tar"
    }
    Write-Output "Downloading portaudio..."
    (New-Object System.Net.WebClient).DownloadFile($portaudio_url, "$PWD\$portaudio_archive")
    7z x $portaudio_archive; if (!$?) {
        Write-Output "Could not extract $portaudio_archive."; exit 1
    }
    7z x "$portaudio_name.tar"; if (!$?) {
        Write-Output "Could not extract $portaudio_name.tar."; exit 1
    }
    Remove-Item $portaudio_archive
    Remove-Item "$portaudio_name.tar"
}

Set-Location portaudio
if (!(Test-Path build)) {
    mkdir build
}
Set-Location build
cmake -A x64 -T host=x64 ..
cmake --build . --config Release -- /m
Set-Location ../..


$build_date = Get-Date -Format yyyy_MM_dd
$package_name = "portaudio-$portaudio_version-$build_date"
if (([IO.Directory]::Exists((Join-Path(Get-Location) $package_name)))) {
    Remove-Item -Force -Recurse $package_name
}
mkdir $package_name
Copy-Item portaudio/include -Recurse $package_name
Copy-Item portaudio/build/Release/* $package_name

Set-Location $package_name
7z a -r "../$package_name.7z"
Set-Location ../
Remove-Item -Force -Recurse $package_name
Write-Output "Use `"7z e portaudio.$portaudio_version.7z -o<OutputFolder>`" to unzip."
