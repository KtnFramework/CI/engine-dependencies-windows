$shaderc_version = "2021.2"
$shaderc_extracted_name = "shaderc-$shaderc_version"
$shaderc_archive = "$shaderc_version.zip"
$shaderc_download_target = "$PWD/$shaderc_archive"
$shaderc_url = "https://github.com/google/shaderc/archive/refs/tags/v$shaderc_version.zip"
if (!([IO.Directory]::Exists((Join-Path(Get-Location) 'shaderc')))) {   
    if (Test-Path $shaderc_extracted_name) {
        Remove-Item $shaderc_extracted_name -Recurse -Force
    }
    if (Test-Path $shaderc_archive) {
        Remove-Item $shaderc_archive
    }
    Write-Output "Downloading shaderc..."
    (New-Object System.Net.WebClient).DownloadFile($shaderc_url, $shaderc_download_target); if (!$?) {
        Write-Output "Could not download shaderc."; exit 1
    }
    7z x $shaderc_archive; if (!$?) {
        Write-Output "Could not extract $shaderc_archive."; exit 1
    }
    Remove-Item $shaderc_archive
    Rename-Item $shaderc_extracted_name shaderc
}



Set-Location shaderc
python3 ./utils/git-sync-deps
if (!(Test-Path build)) {
    mkdir build
}
Set-Location build
cmake -A x64 -DSHADERC_SKIP_TESTS=ON -T host=x64 ..
cmake --build . --config Release -- /m
Set-Location ../..



$build_date = Get-Date -Format yyyy_MM_dd
$package_name = "shaderc-$shaderc_version-$build_date"
if (([IO.Directory]::Exists((Join-Path(Get-Location) $package_name)))) {
    Remove-Item -Force -Recurse $package_name
}
mkdir $package_name/libshaderc/
mkdir $package_name/libshaderc_util
Copy-Item shaderc/build/libshaderc/Release/* `
    -Destination $package_name/libshaderc
Copy-Item shaderc/build/libshaderc_util/Release/* `
    -Destination $package_name/libshaderc_util
Copy-Item shaderc/libshaderc/include `
    -Destination $package_name/libshaderc `
    -Recurse
Copy-Item shaderc/libshaderc_util/include `
    -Destination $package_name/libshaderc_util `
    -Recurse

Set-Location $package_name
7z a -r "../$package_name.7z"
Set-Location ..
Remove-Item -Force -Recurse $package_name
Write-Output "Use `"7z e $package_name.7z -o<OutputFolder>`" to unzip."
